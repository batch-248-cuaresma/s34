//express package was imported as express
const express = require("express");

//invoked express package to create a server/api and save it in a variable which we can refert to later to create routes
const app = express();

//variable for port assignment
const port = 4000;

//express.json() is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from our request

//app.use() is a method used to run another function or method for our expressjs api
//it is used to run middlewares

app.use(express.json())

//by applying the option of "extended:true" this allows us to receive information in other data types such as an object whcih we will use throughout our application
app.use(express.urlencoded({extended:true}))

//CREATE A ROUTE IN EXPRESS
	//express has methods corresponding to each HTTP Method


app.get("/",(req,res)=>{

	//res.send uses the express JS Module's method instead to send a response back to the client
	res.send("Hello from ou default ExpressJs GET route!")
})

app.post("/",(req,res)=>{

	// res.send("Hello from ou default ExpressJs POST route!")
	res.send(`Hello ${req.body.firstName} ${req.body.lastName}`)
})

app.put("/",(req,res)=>{
	res.send("Hello from ou default ExpressJs PUT route!")

})

app.delete("/",(req,res)=>{
	res.send("Hello from ou default ExpressJs DELETE route!")

})











let users = [

		{
			username: "cardo_dalisay",
			password:"quiapo"	
		},
		{
			username: "mommy_d",
			password:"nardadarna"	
		},
		{
			username: "kagawad_godbless",
			password:"dingangbato"	
		}
	]


app.post('/signup',(req,res)=>{

	if(req.body.username == "" && req.body.password == "" ){
		res.send(`Connot be empty`)
	}else{
		users.push(req.body)
		res.send(users)
	}
})


app.put("/change-password",(req,res)=>{

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){
			users[i].password = req.body.password;
			message = `User ${req.body.username}'s password has been updated`
			break
		}else{
			message = "User does not exist"
		}

	}
	res.send(message)
})



/*Activity*/




//1.
app.get('/home',(req,res)=>{
	res.send(`Welcome to home page!`)
})
//2.
app.get('/users',(req,res)=>{
	res.send(users)
})
//3.
app.delete("/delete",(req,res)=>{

	let message;

	for(let i = 0; i < users.length; i++){

		if(req.body.username == users[i].username){
			users.splice(i,1);
			message = `User ${req.body.username}'s has been updated delete`
			break
		}else{
			message = "User does not exist"
		}

	}
	res.send(message)
})




//tells our server to listen to the port
app.listen(port,()=>console.log(`Server running at port ${port}`))

